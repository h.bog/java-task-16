import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

public class FileProperties {
	public static void main(String[] args) {
		String command = "";
		String filename = "";
		String searchWord = "";

		ArrayList<String> fileContent = new ArrayList<String>();

		float fileSize = 0;
		int lineCount = 0;
		int wordCount = 0;

		if (args.length < 2 || args.length > 3) {
			System.out.println("----------------------------------"+
				"\nWrite 'open' followed by a specific filename and optional: a search word."
				+"\nHint: doc.txt is a good file name option"
				+ "\n----------------------------------");
		} else {
			command = args[0].toLowerCase();
			filename = args[1];
			switch(command){
				case "open":
					try {
						File file = new File(filename);
						fileSize = file.length();

						Scanner sc = new Scanner(new File(filename));
						while (sc.hasNextLine()){
							String next = sc.nextLine();
							if (next != ""){
								lineCount++;
								fileContent.add(next);
							}							
						}
						sc.close();
					} catch(Exception e) {
						System.out.println("Cannot open the specified file."
											+"\nHint: Check if you have spelled the file name and file path correctly");
						System.exit(0);
					}

					System.out.println("----------------------------------"
										+"\nFilename: " + filename);
					System.out.println("\n"+"File size: " + fileSize + " bytes");
					System.out.println("Number of lines: " + lineCount);

					try{
						searchWord = args[2];
					}catch(Exception e){
						System.out.println("----------------------------------"
										+ "\nNo search word entered, no search is performed."
										+"\nHint: To search in the file, enter a search word after the file name when \nstarting the program."
										+"\n----------------------------------");
			}

					if (searchWord != ""){
						System.out.println("----------------------------------"
										+"\nSearching for: " + searchWord + "\n");
						for (String line : fileContent){
							for(String word : line.split("\u0020")){
								if (word.toLowerCase().contains(searchWord.toLowerCase())){
									wordCount++;
								}
							}
						}

						System.out.println("Word count: " + wordCount 
											+ "\n----------------------------------");
					}

					break;
			}
		}
	}
}

/*
Name of file

File size
Line count
Search for word (ignore case)
Number of occurences of search word
*/